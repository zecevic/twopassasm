#ifndef _TWO_PASS_ASM_H_
#define _TWO_PASS_ASM_H_

#include "OutputElement.h"
#include "VonNeumannArchCodeHandler.h"
#include <iostream>
#include <fstream>
enum Code { Operation, Directive, Section, Label, Comment, CodeError };
class VonNeumannArchCodeHandler; 
class TwoPassAsm 
{
public:

	bool _assembly;
	OutputElement _outputObject;
	VonNeumannArchCodeHandler _archCodeHandler;
	ifstream _inputFile;
	ofstream _outputFile; 
	list<string> _directiveList;
	list<string> _sectionList;

	SectionElement* _currentSection;

	void firstPass() ; 
	void secondPass(); 
	int _pass; 

	void processOperation(list<string> operation);
	void processLabel(list<string> label);
	void processDirective(list<string> directive);
	void processSection(list<string> section); 


	virtual Code decode(string code);

	// This functions work as in GNU assembler
	virtual bool isDirective(string directive);
	virtual bool isSection(string section);
	virtual bool isLabel(string label);
	static int _locationCounter; 
	static int getSymVal(string name, string section) {
		//_outputObject->
	}
	OutputElement* getOutputObject() {
		return &_outputObject; 
	}
	TwoPassAsm(){

		_sectionList.push_back(".text");
		_sectionList.push_back(".data");
		_sectionList.push_back(".bss");

		_directiveList.push_back(".public");
		_directiveList.push_back(".extern");
		_directiveList.push_back(".word");
		_directiveList.push_back(".long");
		_directiveList.push_back(".align");
		_directiveList.push_back(".char");
		_directiveList.push_back(".skip");
		_directiveList.push_back(".end");
		
	}
	~TwoPassAsm(){}
	void process(char* input, char* output) ; 
	bool checkDefined(string expresion); 
	int addRelocation(string symbol, char op); 
	void addSectionRelocation(char op); 
	int addLDCRelocation(string symbol, int location, bool low); 
	bool isMemDirective(string directive);
	int checkSymbolCurrentSection(string symbol); 

};

#endif