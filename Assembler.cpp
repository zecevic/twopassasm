#include "Assembler.h"
#include "Function.h"
Assembler::Assembler()
{
	_sectionList.push_back(".text"); 
	_sectionList.push_back(".data"); 
	_sectionList.push_back(".bss"); 

	_directiveList.push_back(".public"); 
	_directiveList.push_back(".extern"); 
	_directiveList.push_back(".word"); 
	_directiveList.push_back(".long"); 
	_directiveList.push_back(".align"); 
	_directiveList.push_back(".char"); 
	_directiveList.push_back(".skip"); 
	_directiveList.push_back(".end"); 
}
Code Assembler::decode(string code)
{
	{
		if (_archCodeHandler.isOperation(code))
			return Operation;
		else
			if (isDirective(code))
				return Directive;
			else
				if (isSection(code))
					return Section;
				else
					if (isLabel(code))
						return Label;
					else
						if (_archCodeHandler.isComment(code))
							return Comment;
						else
							return CodeError;
	}

}

bool Assembler::isDirective(string directive)
{
	string myDirective = toLowerCase(directive);
	if (isInList(_directiveList, myDirective))
		return true;
	return false;
}

bool Assembler::isSection(string section)
{
	string mySection = toLowerCase(section);

	int pos = mySection.find_last_of('.');
	if (pos != 0)
		mySection = mySection.substr(0, pos);
	if (isInList(_sectionList, mySection))
		return true;
	else
		return false;
}

bool Assembler::isLabel(string label)
{
	if (label[label.size() - 1] == ':')
		return true;
	else return false;
}

bool Assembler::isMemDirective(string directive)
{
	if (directive.compare(".public") == 0 || directive.compare(".extern") == 0)
		return false;
	else
		return true; 
}
