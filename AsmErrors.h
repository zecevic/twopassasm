#pragma once
#include <iostream>
using namespace std;
class AsmError {
public: 
	string errorMsg; 
	int locationCounter; 
	int pass; 
	AsmError(string errorMsg, int locationCounter= 0, int pass= 0); 
	
	friend ostream& operator<<(ostream& os, const AsmError& a) {
    		return os << a.errorMsg<<" on location "<<a.locationCounter<<"on "<<a.pass<<endl; 
//		return os<<a.errorMsg; 
	}

};
class NoInputFile:
	public AsmError
{
public: 
	NoInputFile(string errorMsg, int locationCounter = 0, int pass = 0) : AsmError(errorMsg, locationCounter, pass) {
	}
};
class LabelDefined:
	public AsmError
{
public:
	LabelDefined(string errorMsg, int locationCounter= 0, int pass= 0) : AsmError(errorMsg, locationCounter, pass) {}
	~LabelDefined() {}
};
class InvalidSymbol:
	public AsmError
{
public:
	InvalidSymbol(string errorMsg, int locationCounter= 0, int pass=0) : AsmError(errorMsg, locationCounter, pass) {}
};

class InvalidDirective :
	public AsmError
{
public: 
	InvalidDirective(string errorMsg, int locationCounter=0, int pass=0) : AsmError(errorMsg, locationCounter, pass) {}
};

class InvalidLabel :
	public AsmError
{
public:
	InvalidLabel(string errorMsg, int locationCounter=0, int pass=0) : AsmError(errorMsg, locationCounter, pass) {}
};
class InvalidNumberFormat:
	public AsmError			
{
public:
	InvalidNumberFormat	(string errorMsg, int locationCounter=0, int pass=0) : AsmError(errorMsg, locationCounter, pass) {}
};

class SymbolDefined :
	public AsmError
{
public: 
	SymbolDefined(string errorMsg, int locationCounter=0, int pass=0) : AsmError(errorMsg, locationCounter, pass) {}
}; 
class InvalidSection:
	public AsmError
{
public:
	InvalidSection(string errorMsg, int locationCounter=0, int pass=0) : AsmError(errorMsg, locationCounter, pass) {}
};
class InvalidOperation:
	public AsmError
{
public: 
	InvalidOperation(string errorMsg, int locationCounter=0, int pass=0) : AsmError(errorMsg, locationCounter, pass){}
};
class NoOutputFile:
	public AsmError
{
public: 
	NoOutputFile(string errorMsg, int locationCounter=0, int pass=0) : AsmError(errorMsg, locationCounter, pass) {}
};
