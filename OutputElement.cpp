#include "OutputElement.h"
#include "AsmErrors.h"
#include <iomanip>
#include <sstream>
OutputElement::OutputElement()
{
	SymbolTableElement ste(0, "UND", "UND", false, true, 0, 0); //Ulaz 0 je definisan za undefined sekciju
	_assemblySymbolTable.push_back(ste);
	_numSymb = 1; 
}


OutputElement::~OutputElement()
{
}

void OutputElement::addAssemblySymbol(string name, string section, bool local, bool isSection, int value)
{
	for (list<SymbolTableElement>::iterator it = _assemblySymbolTable.begin(); it != _assemblySymbolTable.end(); ++it)
		if (it->_name == name) { 
			//validna situacija je samo ako je simbol definisan iz .public direktive
			if (it->_section == "UND") {
				it->_section = section; 
				it->_value = value; 
				return; 
			}
			else 
				throw new SymbolDefined("SymbolDefined: Symbol allready defined. Not allowed redefinition " + name);
		}
	int num = _numSymb++; 
	SymbolTableElement ste(num, name, section, local, isSection, value); 
	_assemblySymbolTable.push_back(ste); 
}

bool OutputElement::setGlobalSymbol(string name) {

	for (list<SymbolTableElement>::iterator it = _assemblySymbolTable.begin(); it != _assemblySymbolTable.end(); ++it)
		if (it->_name == name) {
			it->_local = false; 
			return true; 
		}
	return false; 
}

SectionElement* OutputElement::getNextSection()
{
	++_numCurrSec; //Prvi put ce da vrati 0. sekciju 
	return &( _sectionTable.at(_numCurrSec)); 


}


void OutputElement::addRelocation(string sectionName, int offset, string relocation, int reference)
{
	
	for (vector<SectionElement>::iterator it = _sectionTable.begin(); it != _sectionTable.end(); ++it)
		if (it->_name == sectionName) {
			it->_relocationTable.push_back(RelocationTableElement(sectionName, offset, relocation, reference)); 
			return; 
		}
	throw new InvalidSection("InvalidSection: Couldn't find section: "+sectionName+"for relocation"); 


}

int OutputElement::findSymbolValue(string name)
{
	for (list<SymbolTableElement>::iterator it = _assemblySymbolTable.begin(); it != _assemblySymbolTable.end(); ++it)
		if (it->_name == name) {
			return it->_value; 
		}
	throw new InvalidSymbol("SymbolNotDefined: Symbol "+name+"not defined in file"); 
}

int OutputElement::findSymbolNum(string name)
{
	for (list<SymbolTableElement>::iterator it = _assemblySymbolTable.begin(); it != _assemblySymbolTable.end(); ++it)
		if (it->_name == name) {
			return it->_num;
		}
	return -1; // Ako ne postoji!
}


void OutputElement::printSymbolTable(ofstream& outputFile)
{
	string headerSymTable = "NUM\tNAME\tSECTION\t\tLOCAL\tVALUE\tSIZE"; 
	outputFile << "TABLE\n"; 

	outputFile.fill(' ');
	outputFile.width(7);
	outputFile << left <<"NUM";

	outputFile.fill(' ');
	outputFile.width(15);
	outputFile << "NAME";

	outputFile.fill(' ');
	outputFile.width(10);
	outputFile << "SECTION";

	outputFile.fill(' ');
	outputFile.width(10);
	outputFile << "LOCAL"; 

	outputFile.fill(' ');
	outputFile.width(10);
	outputFile << "VALUE";


	outputFile.fill(' ');
	outputFile.width(15);
	outputFile << "SIZE" << '\n'; 
	
	for (list<SymbolTableElement>::iterator it = _assemblySymbolTable.begin(); it != _assemblySymbolTable.end(); it++) {
		outputFile.fill(' '); 
		outputFile.width(7);
		outputFile << left << it->_num; 
	
		outputFile.fill(' '); 
		outputFile.width(15); 
		outputFile << it->_name; 
		
		outputFile.fill(' '); 
		outputFile.width(10); 
		int num = findSymbolNum(it->_section);
		outputFile << num; 

		outputFile.fill(' '); 
		outputFile.width(10); 
		if (it->_local)
			outputFile << "true";
		else
			outputFile << "false"; 

		outputFile.fill(' '); 
		outputFile.width(10); 
		outputFile << it->_value; 


		outputFile.fill(' '); 
		outputFile.width(15); 
		if (!it->_isSection)
			outputFile << 0 << '\n';
		else {
			unsigned int size = getSectionSizeByName(it->_name);
			outputFile << size << '\n';

		}
	}
	outputFile.flush(); 
		
}

bool OutputElement::isLocalSymbol(string symbol)
{
	for (list<SymbolTableElement>::iterator it = _assemblySymbolTable.begin(); it != _assemblySymbolTable.end(); ++it)
		if (it->_name == symbol)
			return it->_local; 
	
	//ako nije definisan
	throw new InvalidSymbol("SymbolNotDefined: Symbol " + symbol + "not defined in file");


}

void OutputElement::printSectionContent(ofstream& outputFile)
{
	int i; 
	outputFile << right; 
	for(vector<SectionElement>::iterator section = _sectionTable.begin() ; section != _sectionTable.end(); ++section)
	{ 
		if (section->_type == ".bss") continue; 
		outputFile << '\n'; 
		outputFile << "#" << section->_name << "\n"; 
		i = 0; 
		for (vector<char>::iterator byte = section->_byteContent.begin(); byte!=section->_byteContent.end(); ++byte)
		{
			unsigned char b = (*byte); 
			i++; 
			outputFile.width(2);
			outputFile.fill('0');
			outputFile<< hex<<(int)(b)<<" "; 
			if (i % 12 == 0)
				outputFile << '\n';
		}
		outputFile << "\n";
	}
	outputFile.flush(); 
}


string OutputElement::getSectionDefined(string symbol)
{
	for (list<SymbolTableElement>::iterator it = _assemblySymbolTable.begin(); it != _assemblySymbolTable.end(); ++it)
		if (it->_name == symbol)
			return it->_section;
	throw new InvalidSymbol("SymbolNotDefined: Symbol " + symbol + "not defined in file");
}

void OutputElement::printRelocation(ofstream& outputFile)
{

	int i = 0;
	for(vector<SectionElement>::iterator section= _sectionTable.begin() ; section != _sectionTable.end(); ++section)
	{
		if (section->_relocationTable.size() == 0) continue; 
		outputFile << '\n'; 
		outputFile << "#rel" << section->_name << "\n";
		outputFile <<"OFFSET\tTYPE\t\tREFF\n" ; 
		for(vector<RelocationTableElement>::iterator relocation= section->_relocationTable.begin() ; relocation != section->_relocationTable.end(); ++relocation)
			outputFile <<dec<< (int)relocation->_offset << '\t' << relocation->_type << '\t' << relocation->_reference << '\n'; 
	}
}

void OutputElement::sortAndFixSymbolTable()
{
	int num = 0; 
	list<SymbolTableElement> newSymbolTable; 
	for (list<SymbolTableElement>::iterator it = _assemblySymbolTable.begin(); it != _assemblySymbolTable.end(); ++it) {
		if (it->_isSection) {
			SymbolTableElement ste; 
			//int num, string name, string section, bool local, bool isSection, int value = 0, int size = 0
			ste._num = num++; 
			ste._name = it->_name; 
			ste._section = it->_section; 
			ste._local = it->_local; 
			ste._isSection = it->_isSection; 
			ste._value = it->_value; 
			ste._size = it->_size; 
			newSymbolTable.push_back(ste); 
		}
	}
	for (list<SymbolTableElement>::iterator it = _assemblySymbolTable.begin(); it != _assemblySymbolTable.end(); ++it) {
		if (!it->_isSection) {
			SymbolTableElement ste;
			//int num, string name, string section, bool local, bool isSection, int value = 0, int size = 0
			ste._num = num++;
			ste._name = it->_name;
			ste._section = it->_section;
			ste._local = it->_local;
			ste._isSection = it->_isSection;
			ste._value = it->_value;
			ste._size = it->_size;
			newSymbolTable.push_back(ste);

		}
	
	}
	_assemblySymbolTable = newSymbolTable;


}

unsigned int OutputElement::getSectionSizeByName(string name)
{
	if (name == "UND") return 0; 
	for (vector<SectionElement>::iterator section = _sectionTable.begin(); section != _sectionTable.end(); ++section) {
		if (section->_name == name)
			return section->_size; 
	}
	return 0;
}
