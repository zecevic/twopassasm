#ifndef  _OUTPUT_H_
#define _OUTPUT_H_


#include <string>
#include <list>
#include <vector>
#include <fstream>

#include "AsmErrors.h"
using namespace std;

enum Relocation { absP, absM };
const string RELOCATION[] = { "R_386_32_PLUS", "R_386_32_MINUS", "R_386_16L", "R_386_16H" };
struct SymbolTableElement {

	int _num;
	string _name;  // mora biti jedinstveno
	string _section; 
	bool _local; 
	int _value;
	int _size; 
	bool _isSection; 

public:
	SymbolTableElement(int num, string name, string section, bool local, bool isSection, int value = 0, int size = 0) :
		_num(num), _name(name), _section(section), _local(local), _isSection(isSection), _value(value), _size(size) {}
	SymbolTableElement() {}

};

struct RelocationTableElement {
	
	string _section; 
	int _offset; 
	string _type; 
	int _reference; 

	RelocationTableElement(string section, int offset, string type, int reference) :
		_section(section),_offset(offset), _type(type), _reference(reference) {}
};
struct SectionElement {
	string _name; 
	int _start; 
	int _end; 
	unsigned int _size; 
	string _type; 
	vector<char> _byteContent; 
	vector<RelocationTableElement> _relocationTable; 
	SectionElement(string name,string type, int start) :
		_name(name), _type(type), _start(start) {
		_relocationTable = vector<RelocationTableElement>(); 
		_size = 0; 
	}
};
 class OutputElement
{
private: 

	list<SymbolTableElement> _assemblySymbolTable;
	list<RelocationTableElement> _relocationTable; 
	vector<SectionElement> _sectionTable; 

	int _numSymb; 
	int _numCurrSec; 
public:
	OutputElement();
	~OutputElement();


	list<SymbolTableElement> getAssemblySymbolTable()  {
		return _assemblySymbolTable; 
	}

	list<RelocationTableElement> getRelocationTable() const {
		return _relocationTable; 
	}
	void addSectionElement(SectionElement section) {
		_sectionTable.push_back(section); 
	}

	void addAssemblySymbol(string name, string section, bool local, bool isSection, int value = 0) ;

	bool setGlobalSymbol(string name);

	SectionElement* getNextSection(); 
	void restartSection() {
		_numCurrSec = -1; 
	}

	//find way for rellocation for all sections - 
	void addRelocation(string section, int offset, string relocation, int reference); 
	
	list<SymbolTableElement> createLinkerSymbolTable() {}
	int findSymbolValue(string name); 
	int findSymbolNum(string name); 
	void printSymbolTable(ofstream& outputFile); 
	bool isLocalSymbol(string symbol); 
	void printSectionContent(ofstream& outputFile); 
	string getSectionDefined(string symbol); 
	void printRelocation(ofstream& outputFile); 
	void sortAndFixSymbolTable(); 

	unsigned int getSectionSizeByName(string name); 
};
#endif // ! _OUTPUT_H_
