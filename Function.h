#ifndef  _FUNCTION_H_
#define _FUNCTION_H_

#include <string>
#include <list>
using namespace std; 
string toLowerCase(string word); 
bool isInList(list<string> myList, string myWord); 
list<string> parseLine(string line); 
int toIntReg(string number);
int from16(string num); 
int from8(string number); 
int from2(string num); 
int from10(string num); 
bool checkDirectiveParams(list<string>); 
string concatenate(list<string> words); 
int getNumOpParam(list<string> operation); 
bool checkExpersion(string expresion); 
string getSectionType(string section); 
int toInt(string number, int bitLength, bool sign); 
bool isNum(string num); 
#endif // ! _FUNCTION_H