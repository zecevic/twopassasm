#include <iostream>
#include <limits.h>
#include <string>
#include <list>
#include "Function.h"
#include "AsmErrors.h"

using namespace std; 

string toLowerCase(string word) {

	for (int i = 0; i < word.length() ; i++)
		if (word[i] >= 'A' && word[i] <= 'Z')
			word[i] += 'a' - 'A'; //word[i] = tolower(word[i]); 
	return word;
}
bool isInList(list<string> myList, string myWord) {

	for (list<string>::iterator it = myList.begin(); it != myList.end(); ++it)
		if ((*it) == myWord)
			return true;
	return false; 
}
bool isSeparator(char c) {
	if (c == ' ' || c == '\t' || c == ',' || c=='\n' || c=='\r' || c=='\r\n' || c=='\r')
		return true;
	else
		return false; 
}
string getWord(string& line) {
	string word; 
	do {
		int start, end;
		for (start = 0; start < line.size() && isSeparator(line[start]); ++start);
		for (end = start; end < line.size() && !isSeparator(line[end]); ++end);
		 word = line.substr(start, end-start);
		line = line.substr(end);
	} while (word.size() == 0 && line.size()>0); 
	return word; 
	
}
list<string> parseLine(string line) {

	list<string> result; 
	string word; 
	while (line.size() > 0) {
		word = getWord(line);
		if (word!="")
		result.push_back(word);
	}
	return result; 
}

bool allNum(string num) {
	for (int i = 0; i < num.length(); ++i)
		if (!(num[i] >= '0' && num[i] <='9'))
			return false; 
	return true; 
}
int toIntReg(string num) {

	///Proveriti da li za oktalne brojeve treba! 
	return from10(num); 
}

int from8(string num)
{
	int number = 0; 
	int fact=1;
	for (int i = num.length()-1, fact = 1; i >= 0; --i, fact*= 8) 
		if (num[i]>='0' || num[i] <='7')
			number += (num[i] - '0')*fact;
		else 
			throw new InvalidNumberFormat("Invalid Number Format with base 8");
	return number;
}

int from16(string num)
{
	int number=0; 
	int fact=1; 
	for (int i = num.length()-1; i >= 0; --i, fact *= 16)
		if (num[i] >= '0' && num[i] <= '9' )
			number += (num[i] - '0')*fact; 
		else 
			if (num[i] >= 'A' && num[i] <= 'F')
				number += (num[i] - 'A' + 10)*fact;
			else
				if (num[i] >= 'a' && num[i] <= 'f')
					number += (num[i] - 'a' + 10)*fact;
				else
					throw new InvalidNumberFormat("Invalid Number Format with base 16");
	return number;
}

int from2(string num)
{
	int number=0;
	int fact = 1; 
	for (int i = num.length()-1; i >= 0; --i, fact *= 2)
		if (num[i] >= '0' && num[i] <= '1')
			number += (num[i] - '0')*fact;
		else
			throw new InvalidNumberFormat("Invalid Number Format with base 2");
	return number;
}

int from10(string num)
{
	int number=0;
	int fact=1;
	for (int i = num.length()-1; i >= 0; --i, fact*= 10)
		number += (num[i] - '0')*fact;
	return number;
}


string concatenate(list<string> words) {
	string line = "";
	while (words.size()) {
		line += words.front();
		words.pop_front();
	}
	return line;
}

int getNumOpParam(list<string> operation)
{
	int num = 0; 
	operation.pop_front(); //Skinemo kod operacije
	while (!operation.empty()) {
		string s = operation.front();
		operation.pop_front(); 
		if (s != ";")
			num++;
		else
			break; 
	}
	return num; 
}

bool checkExpersion(string expresion)
{
	string word = "";
	bool status = true;
	bool labela = true; //Prvi izraz mora da bude 
	bool operation = false; 
	for (int i = 0; i < expresion.size(); i++) {
		if (expresion[i] == ' ') continue;
		if (expresion[i] != '-' && expresion[i] != '+') {
			if (operation)
				operation = false; 
				word += expresion[i];
		}	
		else
			if (!operation)
				operation = true; 
			else 
				return false; 
	}
	if (operation) return false; 
	return true; 
}

string getSectionType(string section)
{
	section = toLowerCase(section); 
	string text = ".text", data = ".data", bss = ".bss"; 

	string type; 
	size_t found = section.find(text); 
	if (found != std::string::npos) {
		type = text;
		section = section.substr(text.size());
		if (section[0] != '.' && section!="")
			throw new InvalidSection("Invalid Section Name and/or Type");
		else
			return type;
	}
	found = section.find(data);
	if (found != std::string::npos)
		type = data;
	section = section.substr(data.size());
	if (section[0] != '.' && section!="")
		throw new InvalidSection("Invalid Section Name and/or Type");
	else
		return type;

	found = section.find(bss);
	if (found != std::string::npos)
		type = bss;
	section = section.substr(bss.size());
	if (section[0] != '.' &&  section!="")
		throw new InvalidSection("Invalid Section Name and/or Type");

	else
		return type;
}

int toInt(string num, int bitLength, bool sign)
{
	long myNum = 0; 
	int max_sec = 0; 
	if (bitLength == 32)
		max_sec = INT_MAX; 
	else
		max_sec = (1 << bitLength) -1;

	int max_imm = (1 << (bitLength-1))-1; 
	int min_imm = -(1 << (bitLength - 1)); 
	

	int fact = 1;

	if (num[0] == '-') {
		// za ovo mora sign = true; decimanlni negativan
		if (!sign) throw new InvalidNumberFormat("Invalid Number Format"); 
		if (allNum(num.substr(1)))
			myNum = -from10(num.substr(1));
		else throw new InvalidNumberFormat("Invalid Number Format"); 

		if (myNum<min_imm)
			throw new InvalidNumberFormat("Invalid Number Value");
		else
			return myNum; 
	}
	else if (allNum(num)) {
		// u pitanju je ili decimalni ili oktalni broj 

		if (num[0] == '0' && num.length() > 1) {
			//u pitanju je oktalni broj 
			myNum = from8(num.substr(1));
			if (sign) {
				// u pitanju je sign broj
				if (myNum > max_imm) {

					// negativan broj 
					if (myNum > max_sec) throw new InvalidNumberFormat("Invalid Number Value");
					myNum = (myNum - max_sec)-1; //my Num ne bi smeo da bude veci od max_sec
					return myNum;
				}
				return myNum;
			}
			else {
				// u pitanju je unsign oktalni  broj 
				if (myNum > max_sec) throw new InvalidNumberFormat("Invalid Numeber Value");
				return myNum;

			}
		}
		else {
		 // decimalni broj pozitivan 
			myNum = from10(num); 
			if (sign) {
				if (myNum > max_imm)
					throw new InvalidNumberFormat("Invalid Number Value");
				else
					return myNum;
			}
			else {
				if (myNum > max_sec )
					throw new InvalidNumberFormat("Invalid Numeber Value");
				else
					return myNum; 

			}

		}

	}
	else if (num[0] == '0' && (num[1] == 'x' || num[1] == 'X')) {
		// u pitanju su heksadecimanli brojevi 
		myNum = from16(num.substr(2));
		if (sign) {
			if (myNum > max_imm) {
				// u pitanju je negativan broj; 
				if (myNum >= max_sec) throw new InvalidNumberFormat("Invalid Numeber Value");
				myNum = (myNum - max_sec)-1;
				return myNum;
			}
			else
				return myNum;
		}
		else {
			//unsigned velicine su u pitanju 
			if (myNum > max_sec) throw new InvalidNumberFormat("Invalid Numeber Value");
			return myNum;
		}
	}
	else if (num[0] == '0' && (num[1] == 'b' || num[1] == 'B')) {
		myNum = from8(num.substr(2));
		if (sign) {
			if (myNum > max_imm) {
				// u pitanju je negativan broj; 
				if (myNum >= max_sec) throw new InvalidNumberFormat("Invalid Numeber Value");
				myNum = (myNum - max_sec)-1;
				return myNum;
			}
			else
				return myNum;
		}
		else {
			//unsigned velicine su u pitanju 
			if (myNum > max_sec) throw new InvalidNumberFormat("Invalid Numeber Value");
			return myNum;
		}
	}
	else throw new InvalidNumberFormat("Invalid Number Format"); 
}

bool isNum(string num)
{
	if (num.length() == 0)
		return false; 

	if (allNum(num))
		return true; 
	
	if (num.length() == 1) return false; 

	if (num.length() == 2) {
		if (num[0] == '0' && num[1] >= '0' && num[1] <= '7')
			return true;
		else
			return false; 

	}
	if (num[0] == '0' && (num[1] == 'x' || num[1] == 'X')) {
		for (int i = 2; i < num.length(); i++) {
			if (num[i] >= '0' && num[i] <= '9') continue; 
			if (num[i] >= 'A' && num[i] <= 'F') continue; 
			if (num[i] >= 'a' && num[i] <= 'f') continue; 
			return false; 
		}
		return true; 
	}
	if (num[0] == '0' && (num[1] == 'b' || num[1] == 'B')) {
		for (int i = 0; i < num.length(); i++) {
			if (num[i] >= '0' && num[i] <= '1') continue; 
			return false; 
		}
		return false; 
	}
	return false; 
}
