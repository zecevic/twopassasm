#include "TwoPassAsm.h"
#include "AsmErrors.h"
#include <string>
#include <vector>
#include <cstddef>

#include <fstream>
#include "Function.h"

using namespace std; 

int TwoPassAsm::_locationCounter = 0;
void TwoPassAsm::firstPass()
{
	_outputObject.restartSection();
	_assembly = true;
	string line;
	_pass = 1;
	try{
		while (_assembly && !_inputFile.eof()) {

			getline(_inputFile, line);
			list<string> result = parseLine(line);
			if (result.empty()) continue;
			Code code = decode(result.front());
			switch (code) {

			case	Comment: break;

			case	Label:
				processLabel(result);
				break;

			case	Directive:
				processDirective(result);
				break;

			case	Operation:

				processOperation(result);
				break;

			case	Section:
				processSection(result);
				break;

			case	CodeError:
				throw new InvalidSymbol("InvalidSymbol: Invalid token: " + result.front());
				break;
			}

		}
	
	} catch (AsmError* a){
		throw new AsmError(a->errorMsg, _locationCounter, _pass); 
	}
	
}


void TwoPassAsm::secondPass()
{
	_outputObject.restartSection(); 
	_currentSection = nullptr; 
	_assembly = true; 
	_pass = 2; 
	string line; 
	try{
		while (_assembly && !_inputFile.eof()) {

			getline(_inputFile, line);
			list<string> result = parseLine(line);
			if (result.empty()) continue;
			Code code = decode(result.front());
			switch (code) {

			case	Comment: break;

			case	Label:
				processLabel(result);
				break;

			case	Directive:
				processDirective(result);
				break;

			case	Operation:

				processOperation(result);
				break;

			case	Section:
				processSection(result);
				break;

			case	Error:
				throw new InvalidSymbol("InvalidSymbol: Invalid token: " + result.front());
				break;
			}

		}
		if (_assembly && _currentSection != nullptr) {
			_currentSection->_end = _locationCounter;
			unsigned int size = _currentSection->_end - _currentSection->_start;
			_currentSection->_size = size;
		}
	}catch(AsmError* a){
		throw new AsmError(a->errorMsg, _locationCounter,_pass); 
	}
}

void TwoPassAsm::processOperation(list<string> operation)
{
	if (_pass == 1) {

		if (!_currentSection || _currentSection->_type.compare(".text")!=0)
			throw new InvalidSection("InvalidSection: InvalidOperation: Operation can be only in .text section "); 
		_locationCounter += _archCodeHandler.getOperationLength(operation.front()); //1st pass
	}
	else {
		vector<unsigned char> opcode; 
		opcode = _archCodeHandler.decodeOperation(operation, this); 
		for (int i = 0; i < _archCodeHandler.getOperationLength(operation.front()); i++)
			_currentSection->_byteContent.push_back(opcode[i]); 
		//Proveriti da li je za bilo koju operaciju na kraju potrebna relokacija
		_locationCounter += _archCodeHandler.getOperationLength(operation.front()); //2nd pass

	}
}
void TwoPassAsm::processLabel(list<string> label)
{
	if (_pass == 1) {

		string section = "UND"; 
		if (_currentSection != nullptr)
			section = _currentSection->_name; 
		string symbol = label.front(); 
		string labelName = symbol; 
		int size = symbol.size(); 
		if (symbol[size - 1] == ':')
			labelName = symbol.substr(0, size - 1); 
		_outputObject.addAssemblySymbol(labelName, section, true, false, _locationCounter); 
		
		label.pop_front(); //izbaciti labelu iz liste, decodovati dalje sta je u njoj 
		if (label.empty()) return; 
		Code code = decode(label.front());

		switch (code) {

			case Operation: 
				processOperation(label); 
				break;

			case Directive: 
				processDirective(label); 
				break; 

			case Label:
					throw new InvalidLabel("InvalidLabel: Can't be more than one label symbol in one line"); //Nije dozvoljeno vise labela u istom redu 
				break; 

			default: 
				throw new InvalidLabel("InvalidLabel: InvalidToken: "+ label.front()+" after label symbol "+labelName); 
		 }

	}
	else {
		//2nd pass 
		label.pop_front(); 
		if (label.empty()) return; 
		Code code = decode(label.front());
		switch (code) {
		
		case Operation: 
			processOperation(label); 
			break; 

		case Directive:
			processDirective(label);
			break;
		}

	}
}
void TwoPassAsm::processDirective(list<string> directive)
{
	///zavisno od  vrste direktive i prolaza obrada se razkikuj
	if (_pass == 1) {

		string directiveName = directive.front();
		directive.pop_front();

		directiveName = toLowerCase(directiveName);
		int size = directive.size();

		// mem direktive zahtevaju da budu u okviru neke sekcije, public i extern ne moraju
		if (isMemDirective(directiveName) && _currentSection == nullptr)
			throw new InvalidDirective("InvalidDirective: Mem.Directive must be inside some socetion ", _locationCounter, _pass);

		if ((size < 1 || size >3) && directiveName.compare(".align") == 0) {
			// align alignNum [,[fill][,max]]
			unsigned int alignNum = 0;
			unsigned char fill = 0;
			unsigned int max = 0;
			unsigned int count = 0;
			try {
				alignNum = toInt(directive.front(), 32, false);
				directive.pop_front();
				switch (size) {
				case 1:
					_locationCounter += (_locationCounter%alignNum);
					break;

				case 2:
					fill = toInt(directive.front(), 8, false);
					_locationCounter += (_locationCounter%alignNum);
					break;

				case 3:
					fill = toInt(directive.front(), 8, false);
					directive.pop_front();
					max = toInt(directive.front(), 32, false);
					count = _locationCounter%alignNum;
					if (count <= max)
						_locationCounter += count;
					break;
				}

			}
			catch (InvalidNumberFormat* e) {
				throw new InvalidDirective("InvalidDirective: Invalid params for align operation", _locationCounter, _pass);
			}
			return; 
		}

		// Videti da li sme da stoji nesto iza ove direktive! 
		if (directiveName.compare(".end") == 0) {
			_assembly = false;
			if (_currentSection != nullptr) {
				_currentSection->_end = _locationCounter;
				unsigned size = _currentSection->_end - _currentSection->_start;
				_currentSection->_size = size;
			}

			_currentSection = nullptr;
			_locationCounter = 0;
			return;
		}


		if (directiveName.compare(".public") == 0) {
			if (size == 0)
				throw new InvalidDirective("InvalidDirective: .public directive must have at least one param", _locationCounter, _pass);
			else
				for (int i = 0; i < size; i++) {
					string symbol = directive.front();
					if (!_outputObject.setGlobalSymbol(symbol)) {
						//znaci da simbol jos nije definisan, jer je ovo prvi prolaz
						_outputObject.addAssemblySymbol(symbol, "UND", false, false, 0);	
					}
					directive.pop_front();
				}
			return;
		
		}
		if (directiveName.compare(".extern") == 0) {
			if (size == 0)
				throw new InvalidDirective("InvalidDirective: .public directive must have at least one param", _locationCounter, _pass);
			else
				for (int i = 0; i < size; ++i) {
					string symbol = directive.front();
					if (_outputObject.setGlobalSymbol(symbol)) 
						throw new InvalidDirective("InvalidDirective: Params in .extern directive can't be redefind in current file", _locationCounter, _pass);
					else
						_outputObject.addAssemblySymbol(symbol, "UND", false, 0);
					directive.pop_front();
				}
			return;
		}

		if (size == 0) size = 1; // U slaucaju da stoji samo .word bez dodatnih 

		if (directiveName.compare(".char") == 0)
			_locationCounter += size * 1; // char = 1B
		else
			if (directiveName.compare(".word") == 0)
				_locationCounter += size * 2; //word = 2b
			else
				if (directiveName.compare(".long") == 0)
					_locationCounter += size * 4; //long = 4b
				else
					if (directiveName.compare(".skip") == 0) {
						if (size == 1) {
							try {

								int num = toInt(directive.front(), 32, false);


								_locationCounter += num;
							}
							catch (InvalidNumberFormat* e) {
								throw new InvalidDirective("InvalidDirective: " + e->errorMsg + " in directive .bss", _locationCounter, _pass);
							}
						}
						else
							throw new InvalidDirective("InvalidDirective: Wrong number of params for directive .bss", _locationCounter, _pass);
					}
					else throw InvalidDirective("IvalidDirective: InvalidToken: " + directiveName, _locationCounter, _pass);

	}
	else {
		//2nd pass
		if (_currentSection == nullptr && isMemDirective(directive.front()))
			throw new InvalidDirective("InvalidDirective: Mem.Directive must be inside some socetion ", _locationCounter, _pass);


		if (directive.front().compare(".end") == 0) {
			_assembly = false;
			if (_currentSection != nullptr) {
				_currentSection->_end = _locationCounter;
				unsigned size = _currentSection->_end - _currentSection->_start;
				_currentSection->_size = size;
			}

			_currentSection = nullptr;
			_locationCounter = 0;
			return;
		}




		if (_currentSection != nullptr && _currentSection->_type == ".bss") return; //A vec iz prvog prolaza znamo njenu velicinu 

		string directiveName = directive.front();
		directive.pop_front();
		directiveName = toLowerCase(directiveName);
		int size = directive.size();


		if ((size < 1 || size >3) && directiveName.compare(".align") == 0) {
			// align alignNum [,[fill][,max]]
			unsigned int alignNum = 0;
			unsigned char fill = 0;
			unsigned int max = 0;
			unsigned int count = 0;
			try {
				alignNum = toInt(directive.front(), 32, false);
				directive.pop_front();
				switch (size) {
				case 1:
					_locationCounter += (_locationCounter%alignNum);
					break;

				case 2:
					fill = toInt(directive.front(), 8, false);
					count = _locationCounter%alignNum;
					for (int i = 0; i < count; i++)
						_currentSection->_byteContent.push_back(fill);
					_locationCounter += (_locationCounter%alignNum);
					break;

				case 3:
					fill = toInt(directive.front(), 8, false);
					directive.pop_front();
					max = toInt(directive.front(), 32, false);
					count = _locationCounter%alignNum;
					if (count <= max) {
						_locationCounter += count;
						for (unsigned int i = 0; i < count; i++)
							_currentSection->_byteContent.push_back(fill);
					}
					break;
				}

			}
			catch (InvalidNumberFormat* e) {
				throw new InvalidDirective("InvalidDirective: Invalid params for align operation", _locationCounter, _pass);
			}
		}


		if (directiveName.compare(".extern") == 0) {
			//vec su ubaceni u tabelu u prvom prolazu

			for (int i = 0; i < size; ++i) {
				string symbol = directive.front();
				string sectionDef = _outputObject.getSectionDefined(symbol);
				if (sectionDef != "UND")
					throw new InvalidDirective("InvalidDirective: Params in .extern directive can't be redefind in current file", _locationCounter, _pass);

				directive.pop_front();
			}
			return; 
		}
		if (directiveName.compare(".public") == 0) {
			if (size == 0)
				throw new InvalidDirective("InvalidDirective: .public directive must have at least one param", _locationCounter, _pass);
			else
				for (int i = 0; i < size; i++) {
					string symbol = directive.front();
					//proveriti da li je simbol vec defenisan, u tabeli postoji iz prvog prolaza,
					//ali sekcija mora da mu bude razlicita od UNDEF
					string sectionDef = _outputObject.getSectionDefined(symbol);
					if (sectionDef == "UND")
						throw new InvalidDirective("InvalidDirective: Params in .public directive must be defind in current file", _locationCounter, _pass);

					directive.pop_front();
				}
			return; 
		}

			//Za ostale direktive ispisati vrednosti u obj fajl

			if (directiveName.compare(".char") == 0) {
				if (size == 0) {
					_currentSection->_byteContent.push_back(0);
					_locationCounter += 1;
				}
				else
					for (int i = 0; i < size; ++i) {
						if (directive.front().compare(";") == 0) break;
						int init = 0;
						try {
							init = toInt(directive.front(), 8, true);
							directive.pop_front();
						}
						catch (InvalidNumberFormat* e) {
							throw new InvalidDirective("InvalidDirective: " + e->errorMsg + " in directive " + directiveName, _locationCounter, _pass);
						}
						char byte = init & 0xFF; ;
						_currentSection->_byteContent.push_back(byte);
						_locationCounter += 1;
					}
			}
			else
				if (directiveName.compare(".word") == 0) {
					if (size == 0) {
						_currentSection->_byteContent.push_back(0);
						_currentSection->_byteContent.push_back(0);
						_locationCounter += 2;
					}
					else
						for (int i = 0; i < size; ++i) {
							if (directive.front().compare(";") == 0) break;
							int init = 0;
							try {
								init = toInt(directive.front(), 16, true);
								directive.pop_front();
							}
							catch (InvalidNumberFormat* e) {
								throw new InvalidDirective("InvalidDirective: " + e->errorMsg + " in directive " + directiveName, _locationCounter, _pass);
							}
							char byte1 = init & 0xFF; //nizi bajt 
							char byte2 = (init >> 8) & 0xFF; //visi bajt 

							_currentSection->_byteContent.push_back(byte1); //little endian, nizi na nizoj adresi 
							_currentSection->_byteContent.push_back(byte2);  // visi na visoj 
							_locationCounter += 2;
						}
				}
				else
					if (directiveName.compare(".long") == 0) {
						if (size == 0) {
							_currentSection->_byteContent.push_back(0); //little endian, nizi na nizoj adresi 
							_currentSection->_byteContent.push_back(0);  // visi na visoj 
							_currentSection->_byteContent.push_back(0); //little endian, nizi na nizoj adresi 
							_currentSection->_byteContent.push_back(0);  // visi na visoj 
							_locationCounter += 4;
						}
						else
							for (int i = 0; i < size; ++i) {
								if (directive.front().compare(";") == 0) break;
								int init = 0;
								try {
									init = toInt(directive.front(), 32, true);
									directive.pop_front();

									char byte1 = init & 0xFF; //1.(najnizi) bajt 
									char byte2 = (init >> 8) & 0xFF; //2. bajt
									char byte3 = (init >> 16) & 0xFF; //3.bajt
									char byte4 = (init >> 24) & 0xFF; //4. bajt najvisi bajt 

									_currentSection->_byteContent.push_back(byte1); //little endian, nizi na nizoj adresi 
									_currentSection->_byteContent.push_back(byte2);  // visi na visoj adresi 
									_currentSection->_byteContent.push_back(byte3);
									_currentSection->_byteContent.push_back(byte4);
									_locationCounter += 4;
								}
								catch (InvalidNumberFormat* e) {

									//Ako nije int onda je proveriti da li je kombinacija labela
									string expresion = directive.front();


									//posto je vec drugi prolaz svi simboli bi trebalo da budu definisani
									bool status = checkDefined(expresion);
									if (!status)
										throw new InvalidDirective("InvalidDirective: Symbol not defined in directive " + directiveName, _locationCounter, _pass); //Ne postoji koriscen simbol

									//Napraviti zapise o relokaciji
									list<string> words;
									string word = "";
									char op = '+';
									long value = 0;
									int v = 0;
									for (unsigned int i = 0; i < expresion.size(); i++) {
										if (expresion[i] == ' ') continue;
										if (expresion[i] == '-' || expresion[i] == '+') {
											if (word == "")
												op = expresion[i];
											else {
												if (word != "*") {
													if (isNum(word)) {
														v = toInt(word, 0, false); 
														if (op == '+')
															value += v;
														else
															value -= v; 
													}
													else {
														v = addRelocation(word, op);
														if (op == '+')
															value += v;
														else
															value -= v;
													}
												}
												else {

													addSectionRelocation(op);
												}


												word = "";
												op = expresion[i];
											}
										}
										else
											word += expresion[i];
									}
									if (word != "*" && word != "") {
										if (isNum(word)) {
											v = toInt(word, 32, false);
											if (op == '+')
												value += v;
											else
												value -= v;
										}
										else {
											v = addRelocation(word, op);
											if (op == '+')
												value += v;
											else
												value -= v;
										}
									
									}


									char byte1 = value & 0xFF; //1.(najnizi) bajt 
									char byte2 = (value >> 8) & 0xFF; //2. bajt
									char byte3 = (value >> 16) & 0xFF; //3.bajt
									char byte4 = (value >> 24) & 0xFF; //4. bajt najvisi bajt 

									_currentSection->_byteContent.push_back(byte1); //little endian, nizi na nizoj adresi 
									_currentSection->_byteContent.push_back(byte2);  // visi na visoj adresi 
									_currentSection->_byteContent.push_back(byte3);
									_currentSection->_byteContent.push_back(byte4);
									_locationCounter += 4;
								}
							}

					}
					else
						if (directiveName.compare(".skip") == 0) {
							if (size == 1) { //Ovo i ne bi trebalo da se provera jer je u prvom prolazu provereno 
								try {

									int num = toInt(directive.front(), 32, false);

									_locationCounter += num;
									for (int i = 0; i < num; i++)
										_currentSection->_byteContent.push_back(0);
								}
								catch (InvalidNumberFormat* ie) {
									throw new InvalidDirective("InvalidDirective: " + ie->errorMsg + " in directive " + directiveName, _locationCounter, _pass);
								}
							}
							else //Takodje i ovaj uslov je vec proveren! Ali neka ga radi uniformnosti koda! 
								throw new InvalidDirective("InvalidDirective: Invalid number of parameters in directive " + directiveName, _locationCounter, _pass);
						}


		}
}

void TwoPassAsm::processSection(list<string> section)
{
	if (_pass == 1) {

		if (_currentSection != nullptr) {
			_currentSection->_end = _locationCounter;
			unsigned int size = _currentSection->_end - _currentSection->_start; 
			_currentSection->_size = size; 

		}
			string type = getSectionType(section.front()); 

			
			string name = concatenate(section);
			_currentSection = new SectionElement(name,type, 0);			
			_outputObject.addSectionElement(*_currentSection); 
			_outputObject.addAssemblySymbol(name, name, true, true, 0); 
			_locationCounter = 0;
			_currentSection->_start = 0;
			_currentSection->_size = 0; 
	}
	else {
		//2nd pass
		if (_currentSection != nullptr) {
			_currentSection->_end = _locationCounter;
			unsigned int size = _currentSection->_end - _currentSection->_start;
			_currentSection->_size = size;

		}
		_currentSection = _outputObject.getNextSection(); //Samo konstatujemo da smo u novoj sekciji, nema neke obrade!
		_locationCounter = 0;
	}
}
void TwoPassAsm::process(char* input, char* output) 
{
	
	if (input == "") 
		throw new NoInputFile("NoInputFile: Input File name must be specefied"); 


	_inputFile.open(input, ios::in); 

	if (!_inputFile.is_open()) 
		throw new NoInputFile("NoInputFile: Couldn't find input file with specified name"); 

	_outputObject =  OutputElement(); 
	firstPass(); 

	_inputFile.close();

	_outputObject.sortAndFixSymbolTable(); 
	_inputFile.open(input, ios::in);
	if (!_inputFile.is_open()) throw new NoOutputFile("NoOutputFile: Ouptput File name must be specefied");
	secondPass();

	_inputFile.close(); 

	if (output == "")
		throw new NoOutputFile("NoOutputFile: Couldn't find output file with specified name "); 

	_outputFile.open(output, ios::out); 


	_outputObject.printSymbolTable(_outputFile); 

	_outputObject.printSectionContent(_outputFile);

	_outputObject.printRelocation(_outputFile); 
	_outputFile.close(); 

}
bool TwoPassAsm::checkDefined(string expresion)
{
	list<string> words;
	string word = "";
	bool first = true; 
	for (unsigned int i = 0; i < expresion.size(); i++) {
		if (expresion[i] == '-' || expresion[i] == '+') {
			if (word == "" && !first)
				return false;
			else {
				first = false; 
				if (word != "*" && word!="") {
					
					int status = _outputObject.findSymbolNum(word); 
					if (status == -1) {
						if (!isNum(word))
							return false;
					}
						word = ""; 
				}
				else
					word = "";
			}
		}
		else
			word += expresion[i]; 
			
	}
	if (word != "" && word != "*") {
		int status = _outputObject.findSymbolNum(word);
		if (status == -1) {
			if (!isNum(word))
				return false;
		}
		else 
			return true;
	}
	else
		if (word == "*")
			return true;
		else
			return false; 
		
		 
}
int TwoPassAsm::addRelocation(string symbol, char op)
{
	//section, offset, Relocation, reference
	//poziva se u drugom prolazu, svi simboli su definisani - ako nisu, baciti izuzetaL!

	// findSymbol vraca Symbol Value 
	// symbol num je reference
	bool local = _outputObject.isLocalSymbol(symbol); 
	int retValue = 0; //vrednost koja se ugradjuje
	int reference; 
	if (local) {
		// ako je lokalni reference se radi u odnosu na sekciju u kojoj je definisan taj simbol
		string sectionDefined = _outputObject.getSectionDefined(symbol); 
		reference = _outputObject.findSymbolNum(sectionDefined);  //za refenrecu se uzima redni broj iz tabele 
		retValue = _outputObject.findSymbolValue(symbol); //vrednost koja se ugradi je trenutna vrednost simbola
	}
	else {
		reference = _outputObject.findSymbolNum(symbol);  //referenca se radi na sam simbol
		retValue = 0; 
	}
	string relocation = ""; 
	//Dodati tipove relockacija u zavisnoti od OPERACIJE!
	if (op == '+')
		relocation = RELOCATION[0];
	else if (op == '-')
		relocation = RELOCATION[1];
	else
		throw new InvalidDirective("InvalidDirective: Invalid operation in expression "+op); 
	_outputObject.addRelocation(_currentSection->_name, _locationCounter, relocation, reference); 
	return retValue; 
}
void TwoPassAsm::addSectionRelocation(char op)
{
	string relocation; 
	if (op == '+')
		relocation = RELOCATION[0];
	else if (op == '-')
		relocation = RELOCATION[1];
	int reference = _outputObject.findSymbolNum(_currentSection->_name); 
	_outputObject.addRelocation(_currentSection->_name, _locationCounter, relocation, reference);


}
int TwoPassAsm::addLDCRelocation(string symbol, int location, bool low)
{
	bool local = _outputObject.isLocalSymbol(symbol);
	int retValue = 0; //vrednost koja se ugradjuje
	int reference;
	if (local) {
		// ako je lokalni reference se radi u odnosu na sekciju u kojoj je definisan taj simbol
		string sectionDefined = _outputObject.getSectionDefined(symbol);
		reference = _outputObject.findSymbolNum(sectionDefined);  //za refenrecu se uzima redni broj iz tabele 
		retValue = _outputObject.findSymbolValue(symbol); //vrednost koja se ugradi je trenutna vrednost simbola
	}
	else {
		reference = _outputObject.findSymbolNum(symbol);  //referenca se radi na sam simbol
		retValue = 0;
	}

	string relocation = ""; 
	if (low)
		relocation = RELOCATION[2];
	else
		relocation = RELOCATION[3]; 
	
	_outputObject.addRelocation(_currentSection->_name, location, relocation, reference);
	if (low)
		return (retValue & 0xFF);
	else {
		retValue = (retValue >> 16) & 0xFF; 
		return retValue; 
	}
}
bool TwoPassAsm::isDirective(string directive)
{
	string myDirective = toLowerCase(directive);
	if (isInList(_directiveList, myDirective))
		return true;
	return false;
}

bool TwoPassAsm::isSection(string section)
{
	string mySection = toLowerCase(section);

	int pos = mySection.find_last_of('.');
	if (pos != 0)
		mySection = mySection.substr(0, pos);
	if (isInList(_sectionList, mySection))
		return true;
	else
		return false;
}

bool TwoPassAsm::isLabel(string label)
{
	if (label[label.size() - 1] == ':')
		return true;
	else return false;
}

bool TwoPassAsm::isMemDirective(string directive)
{
	if (directive.compare(".public") == 0 || directive.compare(".extern") == 0)
		return false;
	else
		return true;
}
int TwoPassAsm::checkSymbolCurrentSection(string symbol)
{
	return _outputObject.findSymbolValue(symbol); 
}

Code TwoPassAsm::decode(string code)
{
	{
		if (_archCodeHandler.isOperation(code))
			return Operation;
		else
			if (isDirective(code))
				return Directive;
			else
				if (isSection(code))
					return Section;
				else
					if (isLabel(code))
						return Label;
					else
						if (_archCodeHandler.isComment(code))
							return Comment;
						else
							return CodeError;
	}

}

