#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <string>
#include "Function.h"
#include "VonNeumannArchCodeHandler.h"
#include "TwoPassAsm.h"
using namespace std;


int main(int argc, char* argv[]){
	
	TwoPassAsm* as = new TwoPassAsm();
	if (argc<3){
		cout<<"Niste uneli dovoljan broj argumenata"; 
		return -1; 
	} 
	char* inputName = argv[1]; 
	char* outputName = argv[2]; 
	try {

		as->process(inputName, outputName);
		
	}
	catch (AsmError* e) {
		cout<<"Doslo je do greske"<<endl;
		cout<<(*e)<<endl;  
		return -2; 
	}
	cout<<"Finished! Status 0"<<endl; 
	system("Pause"); 
	return 0; 
}
