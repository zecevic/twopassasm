#ifndef _MYASSM_H_
#define _MYASSM_H_

#include <iostream>
#include <fstream>
#include <list>
#include "VonNeumannArchCodeHandler.h"
#include "OutputElement.h"
using namespace std; 
class Assembler
{

	 bool _assembly;
	 OutputElement _outputObject;
	 VonNeumannArchCodeHandler _archCodeHandler;
	 ifstream _inputFile;

	 list<string> _directiveList;
	 list<string> _sectionList;

	static SectionElement* _currentSection;
	
public: 
	Assembler(); 

	virtual ~Assembler() {}
	virtual void process(string input, string output="") = 0; 
	virtual Code decode(string code); 

	// This functions work as in GNU assembler
	virtual bool isDirective(string directive);
	virtual bool isSection(string section);
	virtual bool isLabel(string label);

	virtual void processOperation(list<string> operation) = 0;
	virtual void processLabel(list<string> label) = 0;
	virtual void processDirective(list<string> directive) = 0;
	virtual void processSection(list<string> section) = 0;
	virtual bool isMemDirective(string directive); 

};
#endif // !_MYASSM_H

