#include "VonNeumannArchCodeHandler.h"
#include "Function.h"
#include <cstring>
#include <iostream>
#include <utility>
#include <string>
#include <vector>
#include "TwoPassAsm.h"
using namespace std;

bool VonNeumannArchCodeHandler::hasCondition(string operation)
{
	int size = operation.length(); 
	string subs = ""; 
	if (operation[size - 1] == 's')
		subs = operation.substr(size - 3, 2);
	else
		subs = operation.substr(size - 2, 2); 
	return isInList(_conditionList, subs);
}
bool VonNeumannArchCodeHandler::checkOperationParams(list<string> operation)
{
	string op = operation.front(); 
	//Substring za kode radimo u proveri operacije!
	if (isArithmetic(op) || isLogic(op) || isInOut(op) || isLdcLH(op) || isMove(op) || isLdC(op))
		if (getNumOpParam(operation) != 2)
			return false; 
				else
			return true; //Ako je arithmetic LOgic operation  i param ==2
	else
		if (isInterrupt(op))
			if (getNumOpParam(operation) != 1)
				return false;
			else
				return true;
		else
			if (isShift(op))
				if (getNumOpParam(operation) != 3)
					return false;    // PRoveriti da li mov ima 3 parametra
				else
					return true;
			else
				if (isLdSt(op))
					if (!(getNumOpParam(operation) == 2 || getNumOpParam(operation)==3)) //Naci koji je broj parametara
						return false;
					else
						return true;
				else
					if (isCall(op))
						if (!(getNumOpParam(operation) == 1 || getNumOpParam(operation)==2))
							return false;
						else
							return true; 
}
bool VonNeumannArchCodeHandler::isArithmetic(string operation)
{
	operation = parseOpCode(operation); 
	if (operation == "add" || operation == "sub" || operation == "mul" || operation == "div" || operation == "cmp")
		return true;
	else
		return false; 
}
bool VonNeumannArchCodeHandler::isLogic(string operation)
{
	operation = parseOpCode(operation); 
	if (operation == "and" || operation == "or" || operation == "not" || operation == "test" )
		return true;
	else
		return false;
}
bool VonNeumannArchCodeHandler::isInOut(string operation)
{
	operation = parseOpCode(operation); 
	if (operation == "in" || operation == "out")
		return true;
	else
		return false;
}
bool VonNeumannArchCodeHandler::isInterrupt(string operation)
{
	operation = parseOpCode(operation); 
	if (operation == "int")
		return true;
	else
		return false; 
}
bool VonNeumannArchCodeHandler::isLdSt(string operation)
{
	operation = parseOpCode(operation); 
	if (operation == "ldr" || operation == "str")
		return true;
	else
		return false; 

}
bool VonNeumannArchCodeHandler::isShift(string operation)
{
	operation = parseOpCode(operation); 
	if (operation == "shr" || operation == "shl")
		return true;
	else
		return false; 
}
bool VonNeumannArchCodeHandler::isMove(string operation)
{
	operation = parseOpCode(operation);
	if (operation == "mov")
		return true;
	else
		return false;
}
bool VonNeumannArchCodeHandler::isLdcLH(string operation)
{
	operation = parseOpCode(operation);
	if (operation == "ldch" || operation == "ldcl")
		return true;
	else
		return false;
}
bool VonNeumannArchCodeHandler::isCall(string operation)
{
	operation = parseOpCode(operation);
	if (operation == "call")
		return true;
	else
		return false;
}
bool VonNeumannArchCodeHandler::isLdC(string operation)
{
	operation = parseOpCode(operation);
	if (operation == "ldc")
		return true;
	else
		return false;
}
bool VonNeumannArchCodeHandler::isOperation(string operation)
{
	string myOperation = toLowerCase(operation);

	int size = myOperation.length(); 
	if (myOperation[size - 1] == 's') {
		myOperation = myOperation.substr(0, size-1); //Ako postoji slovo s samo ga skinemo!
		size--; 
	}
	if (isInList(_operationList, myOperation))
		return true;
	else {
		if (size >= 4) {
			string myCondition = myOperation.substr(size-2, 2);
			myOperation = myOperation.substr(0, size-2);

			if (isInList(_operationList, myOperation) && isInList(_conditionList, myCondition))
				return true;
			else
				return false;
		}else 
			return false;
	}
}

bool VonNeumannArchCodeHandler::isComment(string comment)
{
	if (comment[0] == ';')  //x86 syntax, arm - @
		return true;
	else
		return false;
}
vector<unsigned char> VonNeumannArchCodeHandler::decodeOperation(list<string> operation, TwoPassAsm* myAsm) {

	bool status = checkOperationParams(operation); 
	vector<unsigned char> opCode; 
	string op = operation.front(); 
	if (!status)
		throw new InvalidOperation("InvalidOperation: Wrong number of params for operation: "+op); //Ako broj parametara nije odgovarajuci 

	if (isArithmetic(op))
		opCode = decodeArithmetic(operation);  //1
	else if (isLogic(op))
		opCode = decodeLogic(operation);
	else if (isInOut(op))
		opCode = decodeInOut(operation);
	else if (isLdcLH(op))
		opCode = decodeLdcLH(operation);
	else if (isInterrupt(op))
		opCode = decodeInterrupt(operation);
	else if (isLdSt(op))
		opCode = decodeLdSt(operation, myAsm);
	else if (isCall(op))
		opCode = decodeCall(operation, myAsm);
	else if (isShift(op))
		opCode = decodeShift(operation);
	else if (isMove(op))
		opCode = decodeMove(operation);
	else if (isLdC(op))
		opCode = decodeLdC(operation, myAsm); 
	return opCode;

}
unsigned char VonNeumannArchCodeHandler::conditionCode(string cond) {
	unsigned char condition = 7;
	if (cond == "eq")	condition = 0;
	else if (cond == "ne")	condition = 1;
	else if (cond == "gt")	condition = 2;
	else if (cond == "ge")	condition = 3;
	else if (cond == "lt")	condition = 4; 
	else if (cond == "le")	condition = 5;
	else if (cond == "al") condition = 7; 
	return condition;
}
string VonNeumannArchCodeHandler::parseOpCode(string operation)
{
	int size = operation.length(); 
	operation = toLowerCase(operation); 
	if (operation[size - 1] == 's') {
		size--; 
		operation = operation.substr(0, size); // Posto smo smanjili velicinu vec
	}
	if (hasCondition(operation)) {
		size -= 2; 
		operation = operation.substr(0, size); 
	}
	return operation; 
}
bool VonNeumannArchCodeHandler::isRegister(string operand) {
	operand = toLowerCase(operand);
	if (operand == "r0" || operand == "r1" || operand == "r2" || operand == "r3" || operand == "r4" || 
		operand == "r5" || operand == "r6" || operand == "r7" || operand == "r8" || operand == "r9" || 
		operand == "r10" || operand == "r11" || operand == "r12" || operand == "r13" || operand == "r14" || 
		operand == "r15" || operand == "pc" || operand == "lr" || operand == "sp" || operand == "psw") return true;
	else
		return false; 
}
bool VonNeumannArchCodeHandler::isSpecRegister(string operand) {
	if (operand == "pc" || operand == "lr" || operand == "sp" || operand == "psw")
		return true;
	else
		return false; 
}
void VonNeumannArchCodeHandler::initialize()
{
	_operationList.push_front("add");
	_operationList.push_front("sub");
	_operationList.push_front("int");
	_operationList.push_front("mul");
	_operationList.push_front("div");
	_operationList.push_front("cmp");
	_operationList.push_front("and");
	_operationList.push_front("or");
	_operationList.push_front("not");
	_operationList.push_front("test");
	_operationList.push_front("ldr");
	_operationList.push_front("str");
	_operationList.push_front("call");
	_operationList.push_front("in");
	_operationList.push_front("out");
	_operationList.push_front("mov");
	_operationList.push_front("shr");
	_operationList.push_front("shl");
	_operationList.push_front("ldch");
	_operationList.push_front("ldcl");
	_operationList.push_front("ldc");


	_conditionList.push_front("eq");
	_conditionList.push_front("ne");
	_conditionList.push_front("gt");
	_conditionList.push_front("ge");
	_conditionList.push_front("lt");
	_conditionList.push_front("le");
	_conditionList.push_front("al");

}
AddrMode VonNeumannArchCodeHandler::decodeAdrMode(string addres)
{	
	int size = addres.length(); 
	if (addres[size - 1] != '+' && addres[size - 1] != '-' && addres[size - 1] != ']' && 
		addres[0]!= '+' && addres[0]!='-' && addres[0]!='[') {
		//u pitanju je Dir; 
		// proveriti da li je u pitanju Postojeca labela!!!!! 
		if (!isRegister(addres))
			return MemDir;
		else
			return AddrMode::Error; 
	}
	else {
		// dozvoljeno [rX] [rx]+ +[rx]  [rx]- -[rx]
		if (addres[0] == '[' && addres[size - 1] == '+' && addres[size - 2] == ']') {
			string reg = addres.substr(1, size - 3);
			reg = toLowerCase(reg); 
			if (isRegister(reg) && reg !="pc")
				return RegPostInc;
			else
				return AddrMode::Error; 
		}
		if (addres[0] == '[' && addres[size - 1] == '-' && addres[size - 2] == ']') {
			string reg = addres.substr(1, size - 3);
			if (isRegister(reg) && reg != "pc")
				return RegPostDec;
			else
				return AddrMode::Error;
		}
		if (addres[0] == '+' && addres[1] == '[' && addres[size - 1] == ']') {
			string reg = addres.substr(2, size - 3);
			if (isRegister(reg) && reg != "pc")
				return RegPreInc;
			else
				return AddrMode::Error;
		}
		if (addres[0] == '-' && addres[1] == '[' && addres[size - 1] == ']') {
			string reg = addres.substr(2, size - 3);
			if (isRegister(reg) && reg != "pc")
				return RegPreDec;
			else
				return AddrMode::Error;
		}
		if (addres[0] == '[' && addres[size - 1] == ']') {
			string reg = addres.substr(1, size - 2);
			if (isRegister(reg))
				return RegDir;
			else
				return AddrMode::Error;
		}
	}

	return AddrMode::Error; 
}
string VonNeumannArchCodeHandler::parseRegisterAdr(string addres)
{
	int size = addres.length(); 
	if (addres[size - 1] != '+' && addres[size - 1] != '-' && addres[size - 1] != ']' && 
		addres[0]!= '+' && addres[0]!='-' && addres[0]!='[') {
		//u pitanju je memDir; 
		// proveriti da li je u pitanju Postojeca labela!!!!! 
			return ""; 
	}
	else {
		// dozvoljeno [rX] [rx]+ +[rx]  [rx]- -[rx]
		if (addres[0] == '[' && (addres[size - 1] == '+' || addres[size-1]=='-') && addres[size - 2] == ']') {
			string reg = addres.substr(1, size - 3);
			if (isRegister(reg))
				return reg;
			else
				return "";

		
		}
		else if ((addres[0] == '+' || addres[0]=='-')&& addres[1] == '[' && addres[size - 1] == ']') {
			string reg = addres.substr(2, size - 3);
			if (isRegister(reg))
				return reg;
			else
				return "";
		}
		else if (addres[0] == '[' && addres[size - 1] == ']') {
			string reg = addres.substr(1, size - 2); 
			if (isRegister(reg))
				return reg;
			else
				return ""; 
		}


	}

	return ""; 
}
vector<unsigned char> VonNeumannArchCodeHandler::decodeArithmetic(list<string> operation)
{
	string op = operation.front();
	operation.pop_front();
	op = toLowerCase(op);
	unsigned char byte4 = 7; //Bezuslovno izvrsavanje
	unsigned char byte3 = 0;
	unsigned char byte2 = 0;
	unsigned char byte1 = 0;
	bool s = false;
	int size = op.length();

	if (op[size - 1] == 's') {
		s = true;
		op = op.substr(0, size - 1);
		size--;
	}
	if (hasCondition(op)) {
		string cond = op.substr(size - 2, 2);
		op = op.substr(0, size - 2);
		byte4 = conditionCode(cond);
	}
	/// Naci sad za koliko mora da se siftuje ovaj condtion da bi bio na tri najvisa bita 
	byte4 <<= 5; // sad su 3 koda za uslov na vrhu 
	if (s)
		byte4 |= 0x10; // posmatrana instrukcija utice na  flagove

	if (op == "add")		byte4 |= 1;
	else if (op == "sub")	byte4 |= 2;
	else if (op == "mul") 	byte4 |= 3;
	else if (op == "div")	byte4 |= 4;
	else if (op == "cmp")	byte4 |= 5; 

	string dst = toLowerCase(operation.front()); //Ovo mora da bude registar 
	operation.pop_front();
	string src = toLowerCase(operation.front());  // Ovo moze da bude registar ili neposredna velicina 

	if (!isRegister(dst))
		throw new InvalidOperation("InvalidOperation: Destionation must be register in operation: "+op);
	if (dst == "psw")
		throw new InvalidOperation("InvalidOperation: PSW is not allowed in operation: "+op);
	if ((dst == "pc" || dst == "lr" || dst == "sp") && (op != "add" && op != "sub"))
		throw new InvalidOperation("Invalid operation: " + dst+" is allowed only with ADD and SUB operation");

	if (dst == "pc")		byte3 = 16;
	else if (dst == "lr")	byte3 = 17;
	else if (dst == "sp")	byte3 = 18;
	else byte3 = toIntReg(dst.substr(1));
	// ako nije nista od ovih onda je neki registar 
	

	byte3 <<= 3; // Stavimo kod registra na pocetak  

	if (!isRegister(src)) {
		byte3 |= 4;  // Maska za immediete

		int imm = 0;
		
		try {
			//sign 
			imm = toInt(src, 18, true);		
		}
		catch (InvalidNumberFormat* e) {
			throw new InvalidOperation("InvalidOperation: Immediate value must be int on 18 bits in operatione "+op);
		}
		
		///Smestanje imm vrednosti u bajtove
		byte1 = imm & 0xFF; 
		byte2 = (imm >> 8) & 0xFF; 
		byte3 |= (imm >> 0x10) & 3; 

	}
	else {

		unsigned char srcReg = 0; 
			  // AKo je src registar kodovati ga
		if (src == "psw")
			throw new InvalidOperation("InvalidOperation: PSW is not allowed in operation: " + op);
		if ((src == "pc" || src == "lr" || src == "sp") && (op != "add" || op != "sub"))
			throw new InvalidOperation("Invalid operation: " + dst + " is allowed only with ADD and SUB operation");


		if (src == "pc")		srcReg = 16;
		else if (src == "lr")	srcReg = 17;
		else if (src == "sp")	srcReg = 18;
		else
			srcReg = toIntReg(src.substr(1)); 

		byte3 |= (srcReg >> 3) & 3; 
		byte2 = (srcReg & 7) << 5; 
	
	}
	vector<unsigned char> bytes(4);
	bytes[3] = byte1; 
	bytes[2] = byte2; 
	bytes[1] = byte3; 
	bytes[0] = byte4; 
	return	bytes; 
}
vector<unsigned char> VonNeumannArchCodeHandler::decodeLogic(list<string> operation) {

	string op = operation.front();
	operation.pop_front();
	op = toLowerCase(op);
	unsigned char byte4 = 7; //Bezuslovno izvrsavanje
	unsigned char byte3 = 0;
	unsigned char byte2 = 0;
	unsigned char byte1 = 0;
	bool s = false;
	int size = op.length();

	if (op[size - 1] == 's') {
		s = true;
		op = op.substr(0, size - 1);
		size--;
	}
	if (hasCondition(op)) {
		string cond = op.substr(size - 2, 2);
		op = op.substr(0, size - 2);
		byte4 = conditionCode(cond);
	}
	/// Naci sad za koliko mora da se siftuje ovaj condtion da bi bio na tri najvisa bita 
	byte4 <<= 5; // sad su 3 koda za uslov na vrhu 
	if (s)
		byte4 |= 0x10; // posmatrana instrukcija utice na  flagove

	if (op == "and")		byte4 |= 6;
	else if (op == "or")	byte4 |= 7;
	else if (op == "not") 	byte4 |= 8;
	else if (op == "test")	byte4 |= 9;


	string dst = toLowerCase(operation.front()); //Ovo mora da bude registar 
	operation.pop_front();
	string src = toLowerCase(operation.front());  // Ovde i ovo mora da bude registar

	if (!isRegister(dst) || !isRegister(src) || dst == "pc" || dst == "lr" || dst == "psw" || src == "pc" || src == "lr" || src == "psw")
		throw new InvalidOperation("InvalidOperation: Destination and Source in Logic operations "+op+" must be register of general purpose");

	if (dst == "sp")	byte3 = 18;
	else byte3 = toIntReg(dst.substr(1));
	// ako nije nista od ovih onda je neki registar 

	byte3 <<= 3; // Stavimo kod registra na pocetak  

	unsigned char srcReg = 0; 
	if (src == "sp") srcReg = 18; 
	else srcReg = toIntReg(src.substr(1)); 


	byte3 |= (srcReg >> 2) & 7; 
	byte2 |= (srcReg & 3) << 6; 
	vector<unsigned char> bytes(4);
	bytes[3] = byte1;
	bytes[2] = byte2;
	bytes[1] = byte3;
	bytes[0] = byte4;
	return	bytes;
}
vector<unsigned char> VonNeumannArchCodeHandler::decodeShift(list<string> operation)
{
	string op = operation.front();
	operation.pop_front();
	op = toLowerCase(op);
	unsigned char byte4 = 7; //Bezuslovno izvrsavanje
	unsigned char byte3 = 0;
	unsigned char byte2 = 0;
	unsigned char byte1 = 0;
	bool s = false;
	int size = op.length();

	if (op[size - 1] == 's') {
		s = true;
		op = op.substr(0, size - 1);
		size--;
	}
	if (hasCondition(op)) {
		string cond = op.substr(size - 2, 2);
		op = op.substr(0, size - 2);
		byte4 = conditionCode(cond);
	}
	/// Naci sad za koliko mora da se siftuje ovaj condtion da bi bio na tri najvisa bita 
	byte4 <<= 5; // sad su 3 koda za uslov na vrhu 
	if (s)
		byte4 |= 0x10; // posmatrana instrukcija utice na  flagove

	byte4 |= 14; // i shL i shR imaju isti operacionu kod

	string dst = toLowerCase(operation.front()); //Ovo mora da bude registar 
	operation.pop_front();
	string src = operation.front();  // Ovde i ovo mora da bude registar
	operation.pop_front();
	string value = operation.front(); 
	int imm = 0; 
	try {
		imm = toInt(value, 5, false);
		 
	}
	catch (InvalidNumberFormat* e) {
		throw new InvalidOperation("InvalidOperation: Immidiate value in operation: "+op+" must be int on 5 bits"); 
	}
	
	if (!isRegister(dst) || !isRegister(src))
		throw new InvalidOperation("InvalidOperation: Destination and Source in operation: "+op+" must be register");


	if (dst == "pc")		byte3 = 16;
	else if (dst == "lr")	byte3 = 17;
	else if (dst == "sp")	byte3 = 18;
	else if (dst == "psw")	byte3 = 19; 
	else byte3 = toIntReg(dst.substr(1));
	// ako nije nista od ovih onda je neki opsti registar 

	byte3 <<= 3; // Stavimo kod registra na pocetak  

	unsigned char srcReg = 0;
	if (src == "pc")		srcReg = 16;
	else if (src == "lr")	srcReg = 17;
	else if (src == "sp")	srcReg = 18;
	else if (src == "psw")	srcReg = 19;
	else srcReg = toIntReg(src.substr(1));

	byte3 |= (srcReg >> 2) & 7;
	byte2 |= (srcReg & 3) << 6;
	

	byte2 |= ((unsigned char)imm) << 1; 


	size = op.length(); 
	if (op[size - 1] == 'l')
		byte2 |= 1; //Podesimo da je u pitanju shift left*/


	vector<unsigned char> bytes(4);
	bytes[3] = byte1;
	bytes[2] = byte2;
	bytes[1] = byte3;
	bytes[0] = byte4;
	return	bytes;
	
}
vector<unsigned char> VonNeumannArchCodeHandler::decodeMove(list<string> operation) {
	string op = operation.front();
	operation.pop_front();
	op = toLowerCase(op);
	unsigned char byte4 = 7; //Bezuslovno izvrsavanje
	unsigned char byte3 = 0;
	unsigned char byte2 = 0;
	unsigned char byte1 = 0;
	bool s = false;
	int size = op.length();

	if (op[size - 1] == 's') {
		s = true;
		op = op.substr(0, size - 1);
		size--;
	}
	if (hasCondition(op)) {
		string cond = op.substr(size - 2, 2);
		op = op.substr(0, size - 2);
		byte4 = conditionCode(cond);
	}
	/// Naci sad za koliko mora da se siftuje ovaj condtion da bi bio na tri najvisa bita 
	byte4 <<= 5; // sad su 3 koda za uslov na vrhu 
	if (s)
		byte4 |= 0x10; // posmatrana instrukcija utice na  flagove

	byte4 |= 14; // i shL i shR imaju isti operacionu kod

	string dst = toLowerCase(operation.front()); //Ovo mora da bude registar 
	operation.pop_front();
	string src = toLowerCase(operation.front());  // Ovde i ovo mora da bude registar
	operation.pop_front();
	

	if (!isRegister(dst) || !isRegister(src) )
		throw new InvalidOperation("InvalidOperation: Destination and Source in operation: " + op + " must be register");



	if (dst == "pc")		byte3 = 16;
	else if (dst == "lr")	byte3 = 17;
	else if (dst == "sp")	byte3 = 18;
	else if (dst == "psw")	byte3 = 19;
	else byte3 = toIntReg(dst.substr(1));
	// ako nije nista od ovih onda je neki opsti registar 

	byte3 <<= 3; // Stavimo kod registra na pocetak  

	unsigned char srcReg = 0;
	if (src == "pc")		srcReg = 16;
	else if (src == "lr")	srcReg = 17;
	else if (src == "sp")	srcReg = 18;
	else if (src == "psw")	srcReg = 19;
	else srcReg = toIntReg(src.substr(1));

	byte3 |= (srcReg >> 2) & 7;
	byte2 |= (srcReg & 3) << 6;

	// Move je ustavri shift za jedno mesto
	vector<unsigned char> bytes(4);
	bytes[3] = byte1;
	bytes[2] = byte2;
	bytes[1] = byte3;
	bytes[0] = byte4;
	return	bytes;
}
vector<unsigned char> VonNeumannArchCodeHandler::decodeLdcLH(list<string> operation)
{
	string op = operation.front();
	operation.pop_front();
	op = toLowerCase(op);
	unsigned char byte4 = 7; //Bezuslovno izvrsavanje
	unsigned char byte3 = 0;
	unsigned char byte2 = 0;
	unsigned char byte1 = 0;
	bool s = false;
	int size = op.length();

	if (op[size - 1] == 's') {
		s = true;
		op = op.substr(0, size - 1);
		size--;
	}
	if (hasCondition(op)) {
		string cond = op.substr(size - 2, 2);
		op = op.substr(0, size - 2);
		byte4 = conditionCode(cond);
	}
	/// Naci sad za koliko mora da se siftuje ovaj condtion da bi bio na tri najvisa bita 
	byte4 <<= 5; // sad su 3 koda za uslov na vrhu 
	if (s)
		byte4 |= 0x10; // posmatrana instrukcija utice na  flagove

	byte4 |= 15; // i ldcl i ldch imaju isti operacioni kod

	string dst = toLowerCase(operation.front()); //Ovo mora da bude registar 
	operation.pop_front();
	string src = operation.front();  // Ovo mora da bude imm velicina
	operation.pop_front();


	if (!isRegister(dst) || isRegister(src)) //DST mora da bude registar od R0-R15, a c mora da bude imm
		throw new InvalidOperation("InvalidOperation: Destination must be register of general purpose and Source must be immidiate value in operation: " + op);


	if (isSpecRegister(dst)) 		
		throw new InvalidOperation("InvalidOperation: Destination must be register of general purpose and Source must be immidiate value in operation: " + op);


	byte3 = toIntReg(dst.substr(1)); //posto mora da bude R0-R15

	byte3 <<= 4; // Stavimo kod registra na pocetak  
	if (op == "ldch")
		byte3 |= 8; //1 za h, 0 za l
	try {
		int16_t imm = toInt(src, 16, false); 
		byte2 = (imm >> 8) & 0xFF; 
		byte1 = (imm & 0xFF); 
	}
	catch (InvalidNumberFormat* ine) {
		throw new InvalidOperation("InvalidOperation: Immidiate value in operation: " + op + " must be int on 16 bits"); 
	}
	
	vector<unsigned char> bytes(4);
	bytes[3] = byte1;
	bytes[2] = byte2;
	bytes[1] = byte3;
	bytes[0] = byte4;
	return	bytes;
}
vector<unsigned char> VonNeumannArchCodeHandler::decodeCall(list<string> operation, TwoPassAsm *myAsm)
{
	// sintaksa call labela labela u istoj sekciji
	// call r0
	// call r1, 5
	string op = operation.front();
	operation.pop_front();
	op = toLowerCase(op);
	int max_imm = (1 << 18) - 1;
	int min_imm = -(1 << 18);
	unsigned char byte4 = 7; //Bezuslovno izvrsavanje
	unsigned char byte3 = 0;
	unsigned char byte2 = 0;
	unsigned char byte1 = 0;
	bool s = false;
	int size = op.length();

	if (op[size - 1] == 's') {
		s = true;
		op = op.substr(0, size - 1);
		size--;
	}
	if (hasCondition(op)) {
		string cond = op.substr(size - 2, 2);
		op = op.substr(0, size - 2);
		byte4 = conditionCode(cond);
	}


	/// Naci sad za koliko mora da se siftuje ovaj condtion da bi bio na tri najvisa bita 
	byte4 <<= 5; // sad su 3 koda za uslov na vrhu 
	if (s)
		byte4 |= 0x10; // posmatrana instrukcija utice na  flagove
	byte4 |= 12; 

	string dst = toLowerCase(operation.front());
	string label = operation.front(); 
	operation.pop_front(); 

	if (isRegister(dst)) {
		// onda je registar i pomeraj - opciono 

		if (dst == "pc")		byte3 = 16;
		else if (dst == "lr")	byte3 = 17;
		else if (dst == "sp")	byte3 = 18;
		else byte3 = toIntReg(dst.substr(1));

		byte3 <<= 3; // Stavimo kod registra na pocetak 
		int imm = 0; 

		if (operation.size() == 1) {
			// ovo mora da bude imm na 19 bita
			try {
				imm = toInt(operation.front(), 19, true);
			}
			catch (InvalidNumberFormat* e) {
				throw new InvalidOperation("InvalidNOPeration: "+e->errorMsg+":  Immidiate value must be int in 19 bits"); 
			}
		}
		if (imm != 0) {
			byte1 = imm & 0xFF; 
			byte2 = (imm >> 8) & 0xFF; 
			byte3 |= (imm >> 16) & 7; 
		}
		
	} else {
		int value = 0; 
		try {
			 value = myAsm->checkSymbolCurrentSection(label);
			 string sectionName = myAsm->_outputObject.getSectionDefined(label); 
			 if (myAsm->_currentSection->_name != sectionName)
				 throw new InvalidOperation("Invalid Operation: Label must be defined in same section as operation: " + op); 
			 int pc = TwoPassAsm::_locationCounter + 8; //PC ima vrednost za dve instrukcije dalje
			 byte3 = 16; //Kod za pc registar 
			 byte3 <<= 3; 
			 int rel =  value-pc; 

			 if (rel <min_imm || rel>max_imm) throw new InvalidOperation("InvalidOperation: Value of label can't fit in 19 bits in operation: "+op); 

			 byte1 = rel & 0xFF;
			 byte2 = (rel>> 8) & 0xFF;
			 byte3 |= (rel >> 16) & 7;
			 
		} catch (InvalidSymbol* e) 
		{ 
			delete e; 
			throw new InvalidOperation("InvalidOperation: "+e->errorMsg+" in operation "+op); 	
		}	

	}
	
	vector<unsigned char> bytes(4);
	bytes[3] = byte1;
	bytes[2] = byte2;
	bytes[1] = byte3;
	bytes[0] = byte4;
	return	bytes;


	
}
vector<unsigned char> VonNeumannArchCodeHandler::decodeLdC(list<string> operation, TwoPassAsm* myAssm)
{
	
vector<unsigned char> bytes; 
	// u mem treba ugraditi ldch i ldcl instrukciju
	//generalno nije bitno kojim redosledmo se radi 
	vector<unsigned char> bytesLow; 
	vector<unsigned char> bytesHigh; 
	list<string> opLow; 
	list<string> opHigh; 
	string op = operation.front();
	operation.pop_front();
	op = toLowerCase(op); //ovde se nalazi operacija 
	string opLdcL = "ldcl"; 
	string opLdcH = "ldch"; 
	string cond = ""; 
	int size = op.length();

	int immLow = 0; 
	int immHigh = 0; 


	if (op[size - 1] == 's') {
		
		if (hasCondition(op)) {
			cond = op.substr(size - 2, 2);
			opLdcL += cond; 
			opLdcH += cond; 
		}

		opLdcL += 's';
		opLdcH += 's';

	}
	else if (hasCondition(op)) {
			cond = op.substr(size - 2, 2);
			opLdcL += cond;
			opLdcH += cond;
		}


	opLow.push_back(opLdcL); 
	opHigh.push_back(opLdcH); 
	string dst = toLowerCase(operation.front()); //Ovo mora da bude registar 
	operation.pop_front();
	string src = toLowerCase(operation.front());  // Ovo mora da bude imm velicina ili labela
	operation.pop_front();


	if (!isRegister(dst) || isRegister(src)) //DST mora da bude registar od R0-R15, a c mora da bude imm
		throw new InvalidOperation("InvalidOperation: Destination must be register og general purpouse and Source immidiate value or Label in operation: "+op);

	if (isSpecRegister(dst))		
		throw new InvalidOperation("InvalidOperation: Destination must be register og general purpouse in operation: " + op);

	opLow.push_back(dst); 
	opHigh.push_back(dst); 

	try {
		int imm = toInt(src, 32, false);
		immHigh = (imm >> 16) & 0xFFFF;
		immLow = (imm & 0xFFFF);
	}
	catch (InvalidNumberFormat* e) {
		delete e; 
		//throw new InvalidDirective("InvalidOperation: "+e->errorMsg+" in operation "+op);
		try {
			int value = myAssm->_outputObject.findSymbolValue(src);
			immLow = myAssm->addLDCRelocation(src, myAssm->_locationCounter + 2, true); 
			//Dodati relokacije za nizih i visih 16 bita
			immHigh = myAssm->addLDCRelocation(src, myAssm->_locationCounter + 6, false); 
		}
		catch (InvalidSymbol * e) {
			delete e; 
			 throw new InvalidDirective("InvalidOperation: " + e->errorMsg + " in operation " + op);
		}
	}

	opLow.push_back(to_string(immLow)); 
	opHigh.push_back(to_string(immHigh)); 
	bytesLow = decodeLdcLH(opLow); 
	bytesHigh = decodeLdcLH(opHigh); 
	for (unsigned int i = 0; i < bytesLow.size(); ++i)
		bytes.push_back(bytesLow[i]); 
	for (unsigned int i = 0; i < bytesHigh.size(); ++i)
		bytes.push_back(bytesHigh[i]); 
	return bytes; 

}
vector<unsigned char> VonNeumannArchCodeHandler::decodeInOut(list<string> operation) {
	string op = operation.front();
	operation.pop_front();
	op = toLowerCase(op);
	unsigned char byte4 = 7; //Bezuslovno izvrsavanje
	unsigned char byte3 = 0;
	unsigned char byte2 = 0;
	unsigned char byte1 = 0;
	bool s = false;
	int size = op.length();


	if (hasCondition(op)) {
		string cond = op.substr(size - 2, 2);
		op = op.substr(0, size - 2);
		byte4 = conditionCode(cond);
	}

	byte4 <<= 5; 
	byte4 |= 13; 
	string dst = toLowerCase(operation.front()); //Ovo mora da bude registar 
	operation.pop_front();
	string src = operation.front();  // Ovde i ovo mora da bude registar
	operation.pop_front();


	if (!isRegister(dst) || !isRegister(src) || isSpecRegister(src) || isSpecRegister(dst))
		throw new InvalidOperation("InvalidOperation: Destination and Source must be registers of general purpuse in operation: "+op); //samo r0-r15

	unsigned char srcReg = toIntReg(src.substr(1)); 
	unsigned char dstReg = toIntReg(dst.substr(1)); 

	byte3 = (dstReg << 4) | srcReg;

	if (op == "in")
		byte2 |= 0x80; 
	vector<unsigned char> bytes(4);
	bytes[3] = byte1;
	bytes[2] = byte2;
	bytes[1] = byte3;
	bytes[0] = byte4;
	return	bytes;

}
vector<unsigned char> VonNeumannArchCodeHandler::decodeInterrupt(list<string> operation)
{
	string op = operation.front();
	operation.pop_front();
	op = toLowerCase(op);
	unsigned char byte4 = 7; //Bezuslovno izvrsavanje
	unsigned char byte3 = 0;
	unsigned char byte2 = 0;
	unsigned char byte1 = 0;
	bool s = false;
	int size = op.length();

	if (op[size - 1] == 's') {
		s = true;
		op = op.substr(0, size - 1);
		size--;
	}
	

	if (hasCondition(op)) {
		string cond = op.substr(size - 2, 2);
		op = op.substr(0, size - 2);
		byte4 = conditionCode(cond);
	}

	byte4 <<= 5;
	byte4 |= 0; //Code za int
	string src = toLowerCase(operation.front()); //Ovo mora da bude broj 
	int ivt = 0; 
	try {
		ivt = toInt(src, 4, false); 
	} catch(InvalidNumberFormat* e) {
		delete e; 
		throw new InvalidOperation("InvalidOperation: Source must be immidiate value - int on 4 bits max"); 
	}

	byte3 = (unsigned char)(ivt << 4); 

	vector<unsigned char> bytes(4);
	bytes[3] = byte1;
	bytes[2] = byte2;
	bytes[1] = byte3;
	bytes[0] = byte4;
	return	bytes;
}
vector<unsigned char> VonNeumannArchCodeHandler::decodeLdSt(list<string> operation, TwoPassAsm *myAsm)
{
	//sintaksa LDR RO, [R1], imm
	// LDR R0, labela 
	// LDR R0, [R1]+ , imm - post inkrement  R0 -> r, R1 -> a
	// LDR R0, +[R1] , imm - da li moze da ima neposlrednu vrednost i post/pre inc/dec

	string op = operation.front();
	operation.pop_front();
	op = toLowerCase(op);

	unsigned char byte4 = 7; //Bezuslovno izvrsavanje
	unsigned char byte3 = 0;
	unsigned char byte2 = 0;
	unsigned char byte1 = 0;
	bool s = false;
	int size = op.length();

	if (op[size - 1] == 's') {
		s = true;
		op = op.substr(0, size - 1);
		size--;
	}


	if (hasCondition(op)) {
		string cond = op.substr(size - 2, 2);
		op = op.substr(0, size - 2);
		byte4 = conditionCode(cond);
	}
	byte4 <<= 5; // sad su 3 koda za uslov na vrhu 

	/*if (s)
		byte4 |= 0x10; // posmatrana instrukcija utice na  flagove  ne treba da se uzima u obzir */

	byte4 |= 10;  // isti kod za ldr i str


	string r = toLowerCase(operation.front()); //Ovo mora da bude registar 
	operation.pop_front();

	string a = operation.front();  // Registar sa post, pre inc/dec notacijom ili Labela
	operation.pop_front();

	if (!isRegister(r)) 
		throw new InvalidOperation("InvalidOperation: R must be register in operation "+op);

	unsigned char codeR = 0;

	if (r == "pc")		codeR = 16;
	else if (r == "lr")	codeR = 17;
	else if (r == "sp")	codeR = 18;
	else if (r == "psw") codeR = 19;
	else codeR = toIntReg(r.substr(1));

	// ako nije nista od ovih onda je neki registar 


	AddrMode mode = decodeAdrMode(a);

	if (mode == Error)
		throw new InvalidOperation("InvalidOperation: Not allowd address mode in operation: "+op);

	unsigned char codeA = 0;
	unsigned char f = 0;
	int imm = 0; 

	if (mode != MemDir) {

		a = parseRegisterAdr(a); // IF a == "" throw new Invalid Operation

		if (!isRegister(a))  throw new InvalidOperation("InvalidOperation: In register-addr-mode A must be register in operation: " + op);
		if (a == "pc")		codeA = 16;
		else if (a == "lr")	codeA = 17;
		else if (a == "sp")	codeA = 18;
		else if (a == "psw") codeA = 19;
		else codeA = toIntReg(a.substr(1));

		switch (mode) {
		case RegPostInc:	f = 2; break;
		case RegPostDec:	f = 3; break;
		case RegPreInc:		f = 4; break;
		case RegPreDec:		f = 5; break;
		default:			f = 0; break;
		}
		if (a == "pc" && f != 0) throw new InvalidOperation("InvalidOperation: When A is PC, can't use incremenet and decrement in operation "+op);
		if (!operation.empty()) {
			try {

				imm = toInt(operation.front(), 10, true);
			
			}
			catch (InvalidNumberFormat* e) {
				throw new InvalidOperation("InvalidOperation: ImmidiateValue must be int on 10 bits max in operation: "+op); 
			}
		}
		

	}
	else {

		try {
			codeA = 16; // a <= pc
			f = 0; 

			int value = myAsm->checkSymbolCurrentSection(a); 
			string sectionName = myAsm->_outputObject.getSectionDefined(a);
			if (myAsm->_currentSection->_name != sectionName)
				throw new InvalidOperation("Invalid Operation: Label must be defined in same section as operation: " + op);
			int pc = TwoPassAsm::_locationCounter + 8;
			imm = value-pc; 

		}
		catch (InvalidSymbol* e) {
			delete e; 
			throw new InvalidOperation("InvalidOperation: "+e->errorMsg+" in operation: "+op);
		}
			   
	}
		   
	byte3 = codeA << 3;  // Stavimo kod registra na pocetak  
	byte3 |= (codeR >> 2) & 7; 
	byte2 = codeR << 6; 
	byte2 |= (f << 3); 
	if (op == "ldr")
		byte2 |= 4; // ldr -> 1, str -> 0
	byte2 |= (imm >> 8) & 3; 
	byte1 = imm & 0xFF; 

	vector<unsigned char> bytes(4);
	bytes[3] = byte1;
	bytes[2] = byte2;
	bytes[1] = byte3;
	bytes[0] = byte4;
	return	bytes;
}

