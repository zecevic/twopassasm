#ifndef _ARCH_CODE_H_
#define _ARCH_CODE_H_
#include <list>
#include <cstring>
#include <vector>
#include <string>
using namespace std; 
enum AddrMode { RegDir, RegPostDec, RegPostInc, RegPreDec, RegPreInc, MemDir, Error};
class TwoPassAsm; 
class VonNeumannArchCodeHandler 
{

private: 
	list<string> _operationList; //put all operation with lower case here on initialization; 
	list<string> _conditionList; //
	list<string> _directiveList; 
	list<string> _sectionList; ///Ova tri  treba da budu konstanta i u konstruktoru da se odrade! 
//	int _locationCounter; 
	int OPERATION_LENGTH; 
protected: 

	bool checkOperationParams(list<string> operation); 
	bool isArithmetic(string operation); //sve ariteticke
	bool isLogic(string operation); //sve logicke operacije
	bool isInOut(string operation); // in i out 
	bool isInterrupt(string operation); // int 
	bool isLdSt(string operation); // ldr i str
	bool isShift(string operation);  // mov, shr, shl
	bool isMove(string operation); 
	bool isLdcLH(string operation); // ldch i ldcl
	bool isCall(string operation); 
	bool isLdC(string operation); 

	unsigned char conditionCode(string cond); 

	string parseOpCode(string operation); 

	vector<unsigned char> decodeArithmetic(list<string> operation); 
	vector<unsigned char> decodeLogic(list<string> operation); 
	vector<unsigned char> decodeInOut(list<string> operation); 
	vector<unsigned char> decodeInterrupt(list<string> operation); 
	vector<unsigned char> decodeLdSt(list<string> operation, TwoPassAsm* myAssm); 
	vector<unsigned char> decodeShift(list<string> operation); 
	vector<unsigned char> decodeMove(list<string> operation); 
	vector<unsigned char> decodeLdcLH(list<string> operation); 
	vector<unsigned char> decodeCall(list<string> operation, TwoPassAsm* myAssm); 
	vector<unsigned char> decodeLdC(list<string> operation, TwoPassAsm* myAssm); 


	AddrMode decodeAdrMode(string addres); 
	string parseRegisterAdr(string addres); 

	bool isRegister(string operand);
	bool isSpecRegister(string operand);
	void initialize();

public:



	VonNeumannArchCodeHandler()
	{
		OPERATION_LENGTH = 4;
		initialize(); 
	}


	~VonNeumannArchCodeHandler() {}
	
	virtual int getOperationLength(string operation)  {
		if (operation == "ldc")
			return 8; 
		else
			return OPERATION_LENGTH;
	}
	
	 bool isOperation(string operation); 
	bool isComment(string comment);

	vector<unsigned char> decodeOperation(list<string> operation, TwoPassAsm* myAssm); 
	bool hasCondition(string operation);
};

#endif